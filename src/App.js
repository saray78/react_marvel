import React, { Component } from 'react';
import './App.css';
import Logo from "./logo";
// import dummyData from './dummyData'
import Card from "./Card/Card";
import Searcher from "./Searcher";
import lscache from 'lscache';

const API_URL = 'http://gateway.marvel.com:80/v1/public';
const APIKEY_QUERYSTRING = 'apikey=408b6d7a1fd0560193bf0a239d279af1';

export default class App extends Component{
    constructor(...args){
        super(...args);
        this.state = {
            initialState: true,
            isLoading: false,
            results: [],
            favs: []
            // results: dummyData
        };
        // this.handleChange = this.handleChange.bind(this);
        // this.handleClick = this.handleClick.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount(){
        const favs = lscache.get('favs') || [];
        this.setState({
            favs: favs
        })
    }



    /*
    * Cuando le das a Go el estado inicial desaparece
    *
    * Te muestra los resultado de la busqueda de la api
    * de Marvel
    *
    *
    * */

    handleSubmit(textToSearch){
        // e.preventDefault();
        console.log('Texto a buscar', textToSearch);
        this.setState({
            initialState: false,
            isLoading: true
        });

        const FETCH_URL = `${API_URL}/characters?nameStartsWith=${textToSearch}&${APIKEY_QUERYSTRING}`;
        console.log(FETCH_URL);

        //cargar antes de fetch
        fetch(FETCH_URL)
            .then(res => res.json())
            .then(res => {
                this.setState({
                    results: res.data.results,
                    isLoading: false,
                });
            })
            .catch(err => console.log(err));
    }

    render(){
        return(
            <div className='container'>
                <Logo isCentered={true}/>

                <Searcher
                    isLoading={this.state.isLoading}
                    onSubmit={this.handleSubmit}/>

                {this.state.initialState &&
                    <p className='has-text-centered'>Por favor, usa el formulario para buscar el personaje</p>}
                <div className='results'>
                    {this.state.results.map(item => {
                        return(
                            <Card
                                isFav={this.state.favs.some(id=> item.id === id)}
                                item={item}
                                key={item.id}/>
                        )
                        })}
                </div>

            </div>
        )
    }
}


