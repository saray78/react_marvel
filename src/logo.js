import React, {Component} from 'react';
import PropTypes from 'prop-types';


export default function Logo(props) {
    //para ponerlo centrado
    const className = props.isCentered ? 'has-text-centered' : '';
    return(
        <div className={className}>
            <img src='marvel-logo.jpeg' alt='presentation'/>
        </div>
    )
}


Logo.propTypes ={
    isCentered: PropTypes.bool
};